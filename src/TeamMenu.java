import javafx.application.Application;
import javafx.event.*;
import javafx.scene.*;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import javafx.stage.*;
import javafx.geometry.*;

public class TeamMenu extends Application {
	// window setup
	Stage stage = null;
	Scene scene = null;
	VBox root = new VBox();

	// Menu attributes
	private MenuBar mBar = new MenuBar();
	private Menu Azhara = new Menu("Azhara");
	private Menu Fred = new Menu("Fred");
	private Menu NanaKwame = new Menu("Nana Kwame");
	private Menu Rishabh = new Menu("Rishabh");
	private Menu Sasha = new Menu("Sasha");
	private Menu Sewen = new Menu("Sewen");
	private Menu Silas = new Menu("Silas");

	// Menu items
	private MenuItem AbtAzhara = new MenuItem("Azhara Bio");
	private MenuItem AbtFred = new MenuItem("Fred's Bio");
	private MenuItem AbtNana = new MenuItem("Nana's Bio");
	private MenuItem AbtRishab = new MenuItem("Rishab's Bio");
	private MenuItem AbtSasha = new MenuItem("Sasha's Bio");
	private MenuItem AbtSewen = new MenuItem("Sewen's Bio");
	private MenuItem AbtSilas = new MenuItem("Silas' Bio");

	// Label and textArea
	private Label you = new Label("About you: ");
	private TextArea taData = new TextArea();

	// buttons
	private Button Exit = new Button("Exit");
	private Button Clear = new Button("Clear");

	// MAIN POINT OF ENTRY
	public static void main(String[] args) {
		launch(args);
	}

	public void start(Stage _stage) throws Exception {
		stage = _stage;
		stage.setTitle("Resonance Git Menu Exercise");

		// Text Area setup
		taData.setPrefHeight(220);
		taData.setPrefWidth(300);
		taData.setEditable(false);
		taData.setWrapText(true);

		// Menu setup and additions
		Azhara.getItems().addAll(AbtAzhara);
		Fred.getItems().addAll(AbtFred);
		NanaKwame.getItems().addAll(AbtNana);
		Rishabh.getItems().addAll(AbtRishab);
		Sasha.getItems().addAll(AbtSasha);
		Sewen.getItems().addAll(AbtSewen);
		Silas.getItems().addAll(AbtSilas);

		// Flowpane
		FlowPane tArea = new FlowPane();
		tArea.setPadding(new Insets(-15, 0, 0, 0));
		tArea.setAlignment(Pos.CENTER);
		tArea.getChildren().addAll(taData);

		VBox yBox = new VBox();
		yBox.setPadding(new Insets(5, 0, 0, 5));
		yBox.getChildren().add(you);

		HBox btns = new HBox(8);
		btns.setPadding(new Insets(10, 10, 0, 0));
		btns.setAlignment(Pos.BASELINE_RIGHT);
		btns.getChildren().addAll(Clear, Exit);

		mBar.getMenus().addAll(Azhara, Fred, NanaKwame, Rishabh, Sasha, Sewen, Silas);
		root.getChildren().addAll(mBar, yBox, tArea, btns);

		/*
		 * NEXT EVENT HANDLER GOES UNDERNEATH THIS COMMENT HERE!
		 */

		/* FRED HAS PASSED IN AN EVENT HANDLER USING A SHORTCUT METHOD... */ /*
																				 * comment
																				 * by
																				 * Nana
																				 */

		Clear.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent evt) {
				Clear();
			}
		});

		Exit.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent evt) {
				System.exit(0);
			}
		});

		/*
		 * Event handlers for the Fred menu item[ ### COPY AND PASTE THIS BLOCK
		 * OF CODE UNDERNEATH THIS COMMENT AND JUST CHANGE THE TWO OCCURRENCES
		 * OF FRED TO YOUR NAME ###]
		 */
		AbtFred.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent evt) {
				AboutFred();
			}
		});

		/*
		 * [Typed here by Nana]
		 */
		AbtNana.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent evt) {
				AboutNana();
			}
		});

		AbtAzhara.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent evt) {
				AboutAzhara();
			}
		});
		
		AbtSilas.setOnAction(new EventHandler<ActionEvent>() {
			public void handle(ActionEvent evt) {
				AboutSilas();
			}	
		});

		/*
		 * [TYPE IN THE NEXT EVENT HANDLER ABOVE THIS COMMENT OR IMMEDIATELY
		 * UNDER THE LAST SEEN METHOD]]
		 */

		// run the scene
		scene = new Scene(root, 465, 300);
		stage.setScene(scene);
		stage.show();
	}

	/*
	 * About me method where each member has to hardcode an brief bio about
	 * themselves with the setText function
	 */
	public void AboutFred() {
		taData.setText("My name is Fred, I'm a Software Engineering Major at Rochester Institute of Technology in "
				+ "Rochester, New York. I'm 19 years old and Chelsea Football Club is the best thing in my life. My "
				+ "skillset includes: HTML,CSS, JavaScript, PHP, Python, C, Java, mySQL.");
	}

	/*
	 * this is my about method that gets invoked upon an action event on the
	 * Menu 'option' called Nana's Bio.
	 */
	public void AboutNana() {

		taData.setText(
				"My name is Nana Kwame. I am an Electrical and Computer Engineering major at the University of Rochester \n"
						+ "I'm 20 years old, I love innovating, connecting with people, learning and thinking"
						+ "I'm mostly proficient in Java, MySQL,\n" + "HTML, CSS and anything I can. "
						+ "I love Resonance and I just want us to do great things, ALL OF US");
	}

	public void AboutAzhara() {
		taData.setText(
				"My name is Azhara. I'm going to study Computing and Artificial Intelligence at Imperial College London "
				+"starting this October. I'm 18 years old and apart from programming I have another passion which is "
				+ "dancing street dance styles such as house, hip hop, locking and dancehall! "
				+ "My skillset includes: Java, MySQL, HTML, CSS.");
	}
	
	public void AboutSilas() {
		taData.setText(
				"My name is Silas. I am a Mechanical Engineering major at the Kwame Nkruman University of Science and Technology"
				+"I'm 20 years, and I love learning"
				+ "My skillset includes: HTML, CSS, Javascript, Python, MySQL.");
	}


	public void AboutSasha() {
		taData.setText("Hi, My name is Sasha Ena Ofori, I'm a Computer Science Major at Ashesi University in Ghana "
				+ "I'm 19 years old and I love anything that has to do with buying and selling."
				+ " I also enjoy alot of Jazz music when 'chilling' and my favorite instrument is the Sax." 
				+ " I think it is a very sexy instrument and I am currently looking forward to learning very soon"
				+ "skillset includes: HTML,CSS, Python, Java, mySQL.");
	}
	
	
	/*
	 * [WRITE YOUR ABOUT METHOD ABOVE THIS COMMENT, IT SHOULD BE THE SAME AS THE
	 * AboutFred() METHOD BUT WITH YOUR NAME]
	 */

	public void Clear() {
		taData.setText("");
	}

}
